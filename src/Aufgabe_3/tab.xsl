<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <body>
  <h2>Systematics of parrots</h2>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th style="text-align:left">Bezeichnung</th>
        <th style="text-align:left">Gattungsgruppe</th>
        <th style="text-align:left">Gattung</th>
        <th style="text-align:left">Art</th>
      </tr>
      	<xsl:for-each select="Bezeichnung">
      		<xsl:for-each select="Gattungsgruppe">
      			<xsl:for-each select="Gattung">
      				<xsl:for-each select="Art">
      					<tr>
        					<td><xsl:value-of select="../../../@name"/></td>
        					<td><xsl:value-of select="../../@name"/></td>
        					<td><xsl:value-of select="../@name"/></td>
        					<td><xsl:value-of select="@name"/></td>
      					</tr>
      	</xsl:for-each></xsl:for-each></xsl:for-each></xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>

