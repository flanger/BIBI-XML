package Aufgabe_2;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XML_DocBuilder 
{
	public static void main(String[] args) 
	{
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document document = docBuilder.newDocument();
			Element root = document.createElement("Bezeichnung");
			root.setAttribute("name", "Aves");
			document.appendChild(root);
			
			Element gattungsgruppe = document.createElement("Gattungsgruppe");
			gattungsgruppe.setAttribute("name", "Psittaculini");
			root.appendChild(gattungsgruppe);
			
			Element gattung = document.createElement("Gattung");
			gattung.setAttribute("name", "Agapornis");
			gattungsgruppe.appendChild(gattung);
			
			Element art1 = document.createElement("Art");
			art1.setAttribute("name", "Agapornis fisheri");
			gattung.appendChild(art1);
			
			Element art2 = document.createElement("Art");
			art2.setAttribute("name", "Agapornis personatus");
			gattung.appendChild(art2);
			
			Element art3 = document.createElement("Art");
			art3.setAttribute("name", "Agapornis lilianae");
			gattung.appendChild(art3);
			
			Element gattungsgruppe1 = document.createElement("Gattungsgruppe");
			gattungsgruppe1.setAttribute("name", "Psittacini");
			root.appendChild(gattungsgruppe1);
			
			Element gattung1 = document.createElement("Gattung");
			gattung1.setAttribute("name", "Psittacus");
			gattungsgruppe1.appendChild(gattung1);
			
			Element art4 = document.createElement("Art");
			art4.setAttribute("name", "Psittacus erithacus");
			gattung1.appendChild(art4);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(new File("C:/Users/Florian/workspace/XML/src/Aufgabe_2/XML_DOC.xml"));
			transformer.transform(source, result);
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
