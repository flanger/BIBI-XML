package ALGO;
import java.util.Scanner;

public class Palindrom
{
	public static void main (String[] args){
		Scanner s = new Scanner(System.in); 
		System.out.println("Bitte geben Sie ein Wort oder einen Satz ein um zu überprüfen, ob es sich um ein Palindrom handelt.");
		String str = s.nextLine(); 
		if(isPalindrom(str))
		{
			System.out.println("Das Wort bzw. der Satz ist ein Palindrom!");
		} else
		{
			System.out.println("Das Wort bzw. der Satz ist kein Palindrom!");
		}
	}

	private static boolean isPalindrom(String str) {
		// TODO Auto-generated method stub
		str = str.toLowerCase().replace(" ", "").replace(".", "").replace(",", "").replace("!", "").replace("-",""); 
		if(str.length() > 1)
		{
			if (str.charAt(0)== str.charAt(str.length()-1)){
				return isPalindrom(str.substring(1, str.length()-1)); 
			} else 
			{
				return false; 
			}
		}
		return true; 
	}
}
