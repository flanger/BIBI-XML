package ALGO;
import java.util.Scanner;

public class Prim 
{
	//Dieser Algorythmus ber�cksichigt, 
	//dass alle Zahlen, die weder durch 2 noch durch 5 teilbar sind,  
	//auch nicht durch alle gr��eren Zahlen teilbar sind, die mit 0,2,4,5,6, oder 8 enden. 
	//In der Schleife m�ssen also nach 2 und 2 nur noch diejenigen Zahlen 
	//ausprobiert werden, die mit den Ziffern 1,3,7 und 9 enden.
	static boolean isPrim(int value)
	{
		if (value <= 16) { 
            System.out.println(value);
			return (value == 2 || value == 3 || value == 5 || value == 7 || value == 11 || value == 13);
        } 
        if (value % 2 == 0 || value % 3 == 0 || value % 5 == 0 || value % 7 == 0) { 
            return false; 
        } 
        for (long i = 10; i * i <= value; i += 10) { 
            if (value % (i+1) == 0) {  // 11, 21, 31, 41, 51, ... 
                return false; 
            } 
            if (value % (i+3) == 0) {  // 13, 23, 33, 43, 53, ... 
                return false; 
            } 
            if (value % (i+7) == 0) {  // 17, 27, 37, 47, 57, ... 
                return false; 
            } 
            if (value % (i+9) == 0) {  // 19, 29, 39, 49, 59, ... 
                return false; 
            } 
        } 
        return true;
	}
	
	static void prim(int bereich)
	{
		System.out.println("Es werden die Primzahlen bis " + bereich + "berechnet!");
		int anz = 0; 
		for (int i = 0; i <= bereich; i++)
		{
			if (isPrim(i))
			{
				System.out.println(i + " ist eine Primzahl!");
				anz++; 
			}
			else
			{
				System.out.println(i);
			}
		}
		System.out.println("Unter " + bereich + " Zahlen sind " + anz + " Primzahlen!");
		
	}
	
	public static void main(String[] args)
	{
		Scanner s = new Scanner(System.in); 
		int bereich = s.nextInt(); 
		final long start = System.currentTimeMillis(); 
		prim(bereich);
		final long end = System.currentTimeMillis(); 
		System.out.println("Ben�tigte Zeit f�r die Berechnung: " + (double)(end-start)/1000 + " Sekunden!");
	}
}
