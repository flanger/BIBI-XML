package ALGO;
import java.util.Scanner;

public class Fibonacci 
{
	public static int fibonacci(int stelle) //Fibonacci-Berechnung mit Rekursion
	{
		if (stelle == 0){
			return 0; 
		} else if (stelle == 1){
			return 1; 
		} else{
			return fibonacci(stelle-2)+fibonacci(stelle-1); 
		}
	}
	
	public static void main (String[] args)
	{
		int[] arr; System.out.println("Bitte geben Sie einen Wert zur Berechnung der Fibonaccifolge ein!");
		Scanner s = new Scanner(System.in); 
		int abc = s.nextInt(); arr = new int[abc+1];  //Zahl einlesen und int-Array initialisieren
		for (int i = 0; i < arr.length; i++)
		{
			arr[i] = fibonacci(i); //Array bef�llen und ausgeben!
			System.out.println("Fibonacciwert an Stelle " + i + " lautet: " + arr[i]); 
		}
	}
}
