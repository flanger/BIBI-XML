package Aufgabe_1;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class XML_CSV {
	private static BufferedReader fr;
	private static FileWriter writer;

	public static void main(String[] args) throws IOException {
		fr = new BufferedReader(new FileReader(new File("Daten.csv")));
		writer = new FileWriter(new File("Parrot.xml"));
		String[] split;
		String s;
		write(); 
		writer.flush(); 
	}
	
	
	private static ArrayList<String> csvEinlesen() throws FileNotFoundException
	{
		ArrayList<String> list = new ArrayList<>(); 
		Scanner s = new Scanner(new File("C:/Users/Florian/Documents/HTL/5aHWI/BIBI/BIBI - XML/Daten.csv")); 
		while (s.hasNextLine())
		{
			list.add(s.nextLine()); 
		}
		s.close(); 
		return list; 
	}
	
	public static void write () throws IOException
	{
		ArrayList<String> list = csvEinlesen(); 
		writer.write("<? xml version=\"1.0\" encoding=\"UTF-8\" ?>\r");
		writer.write("<Aves>\n"); 
		for (int i = 0; i < list.size(); i++)
		{
			String aktZeile = list.get(i);
			String name = aktZeile.split(";")[0];
			String spez = aktZeile.split(";")[1]; 
			writer.write("\t<" + name + ">" + spez + "</" + name + ">\n");
			
		}
		writer.write("</Aves>");
	}
	
}