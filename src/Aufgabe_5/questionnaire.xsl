<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">

<html>
	<body>
		<u style="color:lightgreen;"><span style="color:#40FF00;font-size:30px"><xsl:value-of select='questionnaire/header/label'/></span></u><br/><br/>
        <xsl:for-each select='//.'>
        
		<xsl:if test="name()='closedEndedQ'">
			<h3><xsl:value-of select='@id'></xsl:value-of>. 
			<xsl:value-of select='text'></xsl:value-of><br/>
			<xsl:value-of select='qdescription'></xsl:value-of></h3>
			<xsl:for-each select='choices'>
				<xsl:choose>
				
					<xsl:when test="@mult='true'">
						<xsl:for-each select='choice'>
							<input type='checkbox'/>
							<xsl:value-of select='text'/><br/>
						</xsl:for-each>
					</xsl:when>

					<xsl:when test="@mult='false'">
						<xsl:for-each select='choice'>
							<input type='radio' name=''/>
							<xsl:value-of select='text'/><br/>
						</xsl:for-each>
					</xsl:when>
					
				</xsl:choose>
			</xsl:for-each>
			
		</xsl:if>
		
		<xsl:if test="name()='questionMatrixMult'">
			<h3><xsl:value-of select='@id'></xsl:value-of>. 
			<xsl:value-of select='text'></xsl:value-of><br/>
			<xsl:value-of select='qdescription'></xsl:value-of></h3>
			
			<table border="2">
			<tr>
				<th></th>
				<xsl:for-each select='choices/choice'>
					<th>
						<xsl:value-of select='text'/>
					</th>
				</xsl:for-each>
			</tr>

			<xsl:for-each select='questions/question'>
				<xsl:variable name="id">
					<xsl:value-of select="@id"/>
				</xsl:variable>

				<tr>
					<td><xsl:value-of select='text'/></td>

					<td><input type='radio' name='{id}'/></td>
					<td><input type='radio' name='{id}'/></td>
					<td><input type='radio' name='{id}'/></td>
					<td><input type='radio' name='{$id}'/></td>
					<td><input type='radio' name='{$id}'/></td>
				</tr>
			</xsl:for-each>
			</table>
			
		</xsl:if>
		
		<xsl:if test="name()='openendedMatrixQ'">
			<h3><xsl:value-of select='@id'></xsl:value-of>. 
			<xsl:value-of select='text'></xsl:value-of><br/>
			<xsl:value-of select='qdescription'></xsl:value-of></h3>
			
			<table border="1">
				<th>Beschreibung</th><td>Minuten</td>
				<xsl:for-each select='questions/question'>
					<tr>
						<td><xsl:value-of select='text'/></td>
						<td><input type='textbox'/></td>
					</tr>
				</xsl:for-each>
			</table>
			
		</xsl:if>
		
		</xsl:for-each>
		<input id="submit" name="submit" type="submit" value="Abschicken"></input>
	</body>
</html>
	
	
	
	</xsl:template>
</xsl:stylesheet>